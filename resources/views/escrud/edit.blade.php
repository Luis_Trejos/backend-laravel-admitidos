@extends('plantilla')
@section('contenido')

<div>
<form id="pregrados" method="POST" enctype="multipart/form-data" action="{{url('admitidos',[$estudiantes])}}">
    @csrf
    @method('PUT')
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Nombre del estudiante</label>
      <input type="text" class="form-control" name="nombre"  aria-describedby="emailHelp" value="{{$estudiantes->nombre}}" required>
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Correo Electronico</label>
      <input type="email" class="form-control" name="correo"  aria-describedby="emailHelp" value="{{$estudiantes->correo}}" required>
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Idetificación</label>
      <input type="text" class="form-control" name="identificacion"  aria-describedby="emailHelp" value="{{$estudiantes->identificacion}}" required>
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Seleccione el programa</label>
     
      <select name="id_pregrado" class="form-select" required>
          <option value=""></option>

          @foreach ($pregrados as $row)
          @if($row->id == $estudiantes->id_pregrado)
          <option selected value="{{ $row->id}}">{{ $row->pregrado}}</option>
              @else
              <option value="{{ $row->id}}">{{ $row->pregrado}}</option>
              @endif
          @endforeach
      </select>

  </div>
 
    <button type="submit" class="btn btn-primary">Añadir</button>
  </form>
</div>

@endsection