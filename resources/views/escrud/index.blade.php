@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-12 col-lg-8 offset-8 offset-lg-2">

            <div class="d-grid gap-2 mb-3">
                <button class="btn btn-dark" type="button" data-bs-toggle="modal" data-bs-target="#pregradomodal">
                    <h4>Añadir Pregrago <i class="bi bi-plus-circle"></i></h4>
                </button>
            </div>


            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Correo Electronico</th>
                        <th scope="col">Identifiación</th>
                        <th scope="col">Programa</th>
                        <th scope="col">Editar</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($estudiantes as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{$row->nombre}}</td>
                        <td>{{$row->correo}}</td>
                        <td>{{$row->identificacion}}</td>
                        <td>{{$row->id_pregrado}}</td>
                        <td><a href="{{ url('admitidos',[$row])}}" type="button" class="btn btn-warning"> Editar</a>        </td>
                        <td>
                            <form method="POST" action="{{ url('admitidos',[$row])}}" >
                                @method("delete")
                                @csrf
                                <button  class="btn btn-danger">Eliminar</button>
                            </form>
                            
                        
                        
                        </td>

                    </tr>
                    @endforeach
                        
                    
            
                 
                 
                </tbody>
            </table>
        </div>
    </div>





    

<!-- Modal -->
<div class="modal fade" id="pregradomodal"  data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="staticBackdropLabel">Añadir nuevo estudiante de admitido</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="pregrados" method="POST" enctype="multipart/form-data" action="{{url("admitidos")}}">
              @csrf
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Nombre del estudiante</label>
                <input type="text" class="form-control" name="nombre"  aria-describedby="emailHelp" required>
              </div>
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Correo Electronico</label>
                <input type="email" class="form-control" name="correo"  aria-describedby="emailHelp" required>
              </div>
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Idetificación</label>
                <input type="text" class="form-control" name="identificacion"  aria-describedby="emailHelp" required>
              </div>
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Seleccione el programa</label>
               
                <select name="id_pregrado" class="form-select" required>
                    <option value=""></option>
                    @foreach ($pregrados as $row)
                    <option value="{{ $row->id}}">{{ $row->pregrado}}</option>
                        
                    @endforeach
                </select>

            </div>
           
       
           
            
              <button type="submit" class="btn btn-primary">Añadir</button>
            </form>
  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  
@endsection
