@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-12 col-lg-8 offset-8 offset-lg-2">
            <div >
              <form id="pregrados" method="POST" enctype="multipart/form-data" action="{{url("pregrados",[$pregrado])}}">
                  @method('PUT')
                  @csrf
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label" >Nombre del Pregrado</label>
                    <input type="text" class="form-control" name="pregrado"  aria-describedby="emailHelp" value="{{$pregrado->pregrado}}" required>
                  </div>
                  <div class="form-check">
                      <input class="form-check-input" type="radio" name="estado" id="activo"  value="1" {{$pregrado->activo == 1 ? 'checked' : ''}}checked > 
                      <label class="form-check-label" for="activo">
                          Publicar
                      </label>
                  </div>
                  <div class="form-check">
                      <input class="form-check-input" type="radio" name="estado" id="inactivo" value="0" {{$pregrado->activo == 0 ? 'checked' : ''}}> 
                      <label class="form-check-label" for="inactivo">
                          No publicar
                      </label>
                  </div>
                  <div class="mb-3">
                      <label for="formFile" class="form-label">Selecionar una imagen</label>
                      <input class="form-control" type="file" id="formFile" name="files" accept="image/*">
                    </div>
               
                
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
      
            </div>
         

        </div>



    </div>

