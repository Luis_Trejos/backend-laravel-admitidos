@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-12 col-lg-8 offset-8 offset-lg-2">

            <div class="d-grid gap-2 mb-3">
                <button class="btn btn-dark" type="button" data-bs-toggle="modal" data-bs-target="#pregradomodal">
                    <h4>Añadir Pregrago <i class="bi bi-plus-circle"></i></h4>
                </button>
            </div>


            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Pregrado</th>
                        <th scope="col">Imagen</th>
                        <th scope="col">Editar</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pregrado as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{$row->pregrado}}</td>
                        <td><img src="{{$row->img}}" class="img-thumbnail" alt="..." style="max-width: 150px"></td>
                        <td><a href="{{ url('pregrados',[$row])}}" type="button" class="btn btn-warning"> Editar</a>        </td>
                        <td>
                            <form method="POST" action="{{ url('pregrados',[$row])}}" >
                                @method("delete")
                                @csrf
                                <button  class="btn btn-danger">Eliminar</button>
                            </form>
                            
                        
                        
                        </td>

                    </tr>
                    @endforeach
                        
                    
            
                 
                 
                </tbody>
            </table>


        </div>



    </div>



<!-- Modal -->
<div class="modal fade" id="pregradomodal"  data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="staticBackdropLabel">Añadir nuevo pregrado</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="pregrados" method="POST" enctype="multipart/form-data" action="{{url("pregrados")}}">
            @csrf
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Nombre del Pregrado</label>
              <input type="text" class="form-control" name="pregrado"  aria-describedby="emailHelp" required>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="estado" id="activo" value="1" checked> 
                <label class="form-check-label" for="activo">
                    Publicar
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="estado" id="inactivo" value="0" > 
                <label class="form-check-label" for="inactivo">
                    No publicar
                </label>
            </div>
            <div class="mb-3">
                <label for="formFile" class="form-label">Selecionar una imagen</label>
                <input class="form-control" type="file" id="formFile" name="files" accept="image/*">
              </div>
         
          
            <button type="submit" class="btn btn-primary">Añadir</button>
          </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@endsection
