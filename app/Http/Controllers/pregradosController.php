<?php

namespace App\Http\Controllers;

use App\Models\Pregrados;
use Illuminate\Http\Request;


class pregradosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pregrado = Pregrados::all();
        return view('crudpre.indexpre', compact('pregrado'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $pregrado = new Pregrados();

        if($request->hasFile('files')){
            $file = $request->file('files');
            $desimg = 'img/';
            $filename = time() .'_' . $file->getClientOriginalName();
            $uploadSeccess = $request->file('files')->move($desimg, $filename);
            $url= url($desimg .$filename);
            $pregrado->img = $url;
        }
       
        $pregrado->fill($request->except('activo'))->saveOrfail();
        $pregrado->estado = $request->input('estado', false);
        $pregrado->save();
        return redirect('pregrados');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $pregrado = Pregrados::find($id);
        return view('crudpre.editpre', compact('pregrado'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $pregrado =  Pregrados::find($id);
        $pregrado->fill($request->input())->saveOrfail();

        return redirect('pregrados');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
       $pregrado =  Pregrados::find($id);
       $pregrado->delete();
       return redirect('pregrados');
    }
}
